# ProtonVPN Utility

## Introduction

A small shell utility I created for myself in order to simplify the use of ProtonVPN command lines.
The script will ask for sudo authentification, that's because ProntonVPN needs rights in order to execute.

## Screenshot

<img src="https://framagit.org/Raskm/protonvpn-utility/-/raw/master/Images/protonVPN-Utility-Screenshot.png" alt="Utility Print screen">