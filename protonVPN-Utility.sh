#!/bin/bash
# Author : Axel AGNAN
[ `whoami` = root ] || { sudo "$0" "$@"; exit $?; }

introduction () {
    reset
    echo "ProtonVPN - Utility"
    echo "-------------------"
    protonvpn --version &
    protonvpn s &
    wait
    echo "-------------------"
    menu
}

menu () {
    PS3="Please enter your choice: "
    options=("Select a ProtonVPN server and connect to it"
             "Connect to a random server." 
             "Connect to the fastest server"
             "Connect to the fastest P2P server"
             "Connect to the fastest Secure Core server."
             "Reconnect or connect to the last server used."
             "Disconnect" 
             "Exit")

    select opt in "${options[@]}"
    do
        case $opt in
            "Select a ProtonVPN server and connect to it")
                protonvpn connect &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Connect to a random server.")
                protonvpn c -r &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Connect to the fastest server")
                protonvpn c -f &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Connect to the fastest P2P server")
                protonvpn c --p2p &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Connect to the fastest Secure Core server.")
                protonvpn c --sc &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Reconnect or connect to the last server used.")
                protonvpn reconnect &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Disconnect")
                protonvpn d &
                wait
                read -rsn1 -p"Press any key to continue";echo
                introduction
                ;;
            "Exit")
                break
                exit 0
                ;;
            *) echo "invalid option $REPLY";;
        esac
    done
}
introduction
